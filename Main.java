public class Main {

    public static int fixedPointLinearSearch(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            if (arr[i] == i)
                return arr[i];
        }
        return -1;
    }

    public static int fixedPointBinarySearch(int[] arr, int n) {
        int l = 0, r =  n - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (arr[m] == m ) {
                return m;
            }
            if (arr[m] < m) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
	    int[] arr = {-1, 0, 2, 4, 5, 6, 7};
	    int pos = fixedPointBinarySearch(arr, arr.length);
        System.out.println(pos == -1 ? "Khong ton tai" : pos);
    }
}
